/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.sharemydocs.db.jooq;


import de.jottyfan.sharemydocs.db.jooq.tables.TDocument;
import de.jottyfan.sharemydocs.db.jooq.tables.TDocumentfolder;
import de.jottyfan.sharemydocs.db.jooq.tables.TDocumenttag;
import de.jottyfan.sharemydocs.db.jooq.tables.TFolder;
import de.jottyfan.sharemydocs.db.jooq.tables.TProfile;
import de.jottyfan.sharemydocs.db.jooq.tables.TProfilerole;
import de.jottyfan.sharemydocs.db.jooq.tables.TRole;
import de.jottyfan.sharemydocs.db.jooq.tables.TTag;
import de.jottyfan.sharemydocs.db.jooq.tables.VDocument;
import de.jottyfan.sharemydocs.db.jooq.tables.VDocumentfolder;
import de.jottyfan.sharemydocs.db.jooq.tables.VFolder;
import de.jottyfan.sharemydocs.db.jooq.tables.VProfile;
import de.jottyfan.sharemydocs.db.jooq.tables.VRole;
import de.jottyfan.sharemydocs.db.jooq.tables.VVersion;

import java.util.Arrays;
import java.util.List;

import org.jooq.Catalog;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Public extends SchemaImpl {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>public</code>
     */
    public static final Public PUBLIC = new Public();

    /**
     * The table <code>public.t_document</code>.
     */
    public final TDocument T_DOCUMENT = TDocument.T_DOCUMENT;

    /**
     * The table <code>public.t_documentfolder</code>.
     */
    public final TDocumentfolder T_DOCUMENTFOLDER = TDocumentfolder.T_DOCUMENTFOLDER;

    /**
     * The table <code>public.t_documenttag</code>.
     */
    public final TDocumenttag T_DOCUMENTTAG = TDocumenttag.T_DOCUMENTTAG;

    /**
     * The table <code>public.t_folder</code>.
     */
    public final TFolder T_FOLDER = TFolder.T_FOLDER;

    /**
     * The table <code>public.t_profile</code>.
     */
    public final TProfile T_PROFILE = TProfile.T_PROFILE;

    /**
     * The table <code>public.t_profilerole</code>.
     */
    public final TProfilerole T_PROFILEROLE = TProfilerole.T_PROFILEROLE;

    /**
     * The table <code>public.t_role</code>.
     */
    public final TRole T_ROLE = TRole.T_ROLE;

    /**
     * The table <code>public.t_tag</code>.
     */
    public final TTag T_TAG = TTag.T_TAG;

    /**
     * The table <code>public.v_document</code>.
     */
    public final VDocument V_DOCUMENT = VDocument.V_DOCUMENT;

    /**
     * The table <code>public.v_documentfolder</code>.
     */
    public final VDocumentfolder V_DOCUMENTFOLDER = VDocumentfolder.V_DOCUMENTFOLDER;

    /**
     * The table <code>public.v_folder</code>.
     */
    public final VFolder V_FOLDER = VFolder.V_FOLDER;

    /**
     * The table <code>public.v_profile</code>.
     */
    public final VProfile V_PROFILE = VProfile.V_PROFILE;

    /**
     * The table <code>public.v_role</code>.
     */
    public final VRole V_ROLE = VRole.V_ROLE;

    /**
     * The table <code>public.v_version</code>.
     */
    public final VVersion V_VERSION = VVersion.V_VERSION;

    /**
     * No further instances allowed
     */
    private Public() {
        super("public", null);
    }


    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        return Arrays.asList(
            TDocument.T_DOCUMENT,
            TDocumentfolder.T_DOCUMENTFOLDER,
            TDocumenttag.T_DOCUMENTTAG,
            TFolder.T_FOLDER,
            TProfile.T_PROFILE,
            TProfilerole.T_PROFILEROLE,
            TRole.T_ROLE,
            TTag.T_TAG,
            VDocument.V_DOCUMENT,
            VDocumentfolder.V_DOCUMENTFOLDER,
            VFolder.V_FOLDER,
            VProfile.V_PROFILE,
            VRole.V_ROLE,
            VVersion.V_VERSION
        );
    }
}
