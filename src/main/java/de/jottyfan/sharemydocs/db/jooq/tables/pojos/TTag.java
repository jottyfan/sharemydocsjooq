/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.sharemydocs.db.jooq.tables.pojos;


import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TTag implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Integer       pk;
    private final String        name;
    private final String        prefix;
    private final String        suffix;
    private final LocalDateTime lastchange;

    public TTag(TTag value) {
        this.pk = value.pk;
        this.name = value.name;
        this.prefix = value.prefix;
        this.suffix = value.suffix;
        this.lastchange = value.lastchange;
    }

    public TTag(
        Integer       pk,
        String        name,
        String        prefix,
        String        suffix,
        LocalDateTime lastchange
    ) {
        this.pk = pk;
        this.name = name;
        this.prefix = prefix;
        this.suffix = suffix;
        this.lastchange = lastchange;
    }

    /**
     * Getter for <code>public.t_tag.pk</code>.
     */
    public Integer getPk() {
        return this.pk;
    }

    /**
     * Getter for <code>public.t_tag.name</code>.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter for <code>public.t_tag.prefix</code>.
     */
    public String getPrefix() {
        return this.prefix;
    }

    /**
     * Getter for <code>public.t_tag.suffix</code>.
     */
    public String getSuffix() {
        return this.suffix;
    }

    /**
     * Getter for <code>public.t_tag.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return this.lastchange;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("TTag (");

        sb.append(pk);
        sb.append(", ").append(name);
        sb.append(", ").append(prefix);
        sb.append(", ").append(suffix);
        sb.append(", ").append(lastchange);

        sb.append(")");
        return sb.toString();
    }
}
