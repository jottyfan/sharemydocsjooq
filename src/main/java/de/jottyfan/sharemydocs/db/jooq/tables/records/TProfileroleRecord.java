/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.sharemydocs.db.jooq.tables.records;


import de.jottyfan.sharemydocs.db.jooq.tables.TProfilerole;

import java.time.LocalDateTime;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TProfileroleRecord extends UpdatableRecordImpl<TProfileroleRecord> implements Record4<Integer, Integer, Integer, LocalDateTime> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>public.t_profilerole.pk</code>.
     */
    public TProfileroleRecord setPk(Integer value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>public.t_profilerole.pk</code>.
     */
    public Integer getPk() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.t_profilerole.fk_profile</code>.
     */
    public TProfileroleRecord setFkProfile(Integer value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>public.t_profilerole.fk_profile</code>.
     */
    public Integer getFkProfile() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>public.t_profilerole.fk_role</code>.
     */
    public TProfileroleRecord setFkRole(Integer value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>public.t_profilerole.fk_role</code>.
     */
    public Integer getFkRole() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>public.t_profilerole.lastchange</code>.
     */
    public TProfileroleRecord setLastchange(LocalDateTime value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>public.t_profilerole.lastchange</code>.
     */
    public LocalDateTime getLastchange() {
        return (LocalDateTime) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row4<Integer, Integer, Integer, LocalDateTime> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    @Override
    public Row4<Integer, Integer, Integer, LocalDateTime> valuesRow() {
        return (Row4) super.valuesRow();
    }

    @Override
    public Field<Integer> field1() {
        return TProfilerole.T_PROFILEROLE.PK;
    }

    @Override
    public Field<Integer> field2() {
        return TProfilerole.T_PROFILEROLE.FK_PROFILE;
    }

    @Override
    public Field<Integer> field3() {
        return TProfilerole.T_PROFILEROLE.FK_ROLE;
    }

    @Override
    public Field<LocalDateTime> field4() {
        return TProfilerole.T_PROFILEROLE.LASTCHANGE;
    }

    @Override
    public Integer component1() {
        return getPk();
    }

    @Override
    public Integer component2() {
        return getFkProfile();
    }

    @Override
    public Integer component3() {
        return getFkRole();
    }

    @Override
    public LocalDateTime component4() {
        return getLastchange();
    }

    @Override
    public Integer value1() {
        return getPk();
    }

    @Override
    public Integer value2() {
        return getFkProfile();
    }

    @Override
    public Integer value3() {
        return getFkRole();
    }

    @Override
    public LocalDateTime value4() {
        return getLastchange();
    }

    @Override
    public TProfileroleRecord value1(Integer value) {
        setPk(value);
        return this;
    }

    @Override
    public TProfileroleRecord value2(Integer value) {
        setFkProfile(value);
        return this;
    }

    @Override
    public TProfileroleRecord value3(Integer value) {
        setFkRole(value);
        return this;
    }

    @Override
    public TProfileroleRecord value4(LocalDateTime value) {
        setLastchange(value);
        return this;
    }

    @Override
    public TProfileroleRecord values(Integer value1, Integer value2, Integer value3, LocalDateTime value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TProfileroleRecord
     */
    public TProfileroleRecord() {
        super(TProfilerole.T_PROFILEROLE);
    }

    /**
     * Create a detached, initialised TProfileroleRecord
     */
    public TProfileroleRecord(Integer pk, Integer fkProfile, Integer fkRole, LocalDateTime lastchange) {
        super(TProfilerole.T_PROFILEROLE);

        setPk(pk);
        setFkProfile(fkProfile);
        setFkRole(fkRole);
        setLastchange(lastchange);
    }

    /**
     * Create a detached, initialised TProfileroleRecord
     */
    public TProfileroleRecord(de.jottyfan.sharemydocs.db.jooq.tables.pojos.TProfilerole value) {
        super(TProfilerole.T_PROFILEROLE);

        if (value != null) {
            setPk(value.getPk());
            setFkProfile(value.getFkProfile());
            setFkRole(value.getFkRole());
            setLastchange(value.getLastchange());
        }
    }
}
