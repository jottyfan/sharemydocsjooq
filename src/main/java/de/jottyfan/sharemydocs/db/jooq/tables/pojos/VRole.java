/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.sharemydocs.db.jooq.tables.pojos;


import java.io.Serializable;
import java.util.Arrays;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VRole implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Integer  pk;
    private final String   name;
    private final String[] users;

    public VRole(VRole value) {
        this.pk = value.pk;
        this.name = value.name;
        this.users = value.users;
    }

    public VRole(
        Integer  pk,
        String   name,
        String[] users
    ) {
        this.pk = pk;
        this.name = name;
        this.users = users;
    }

    /**
     * Getter for <code>public.v_role.pk</code>.
     */
    public Integer getPk() {
        return this.pk;
    }

    /**
     * Getter for <code>public.v_role.name</code>.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter for <code>public.v_role.users</code>.
     */
    public String[] getUsers() {
        return this.users;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("VRole (");

        sb.append(pk);
        sb.append(", ").append(name);
        sb.append(", ").append(Arrays.toString(users));

        sb.append(")");
        return sb.toString();
    }
}
